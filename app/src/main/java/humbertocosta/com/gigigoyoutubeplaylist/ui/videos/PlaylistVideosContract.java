package humbertocosta.com.gigigoyoutubeplaylist.ui.videos;

import android.content.Context;

import humbertocosta.com.gigigoyoutubeplaylist.data.models.playlist.PlaylistResponse;
import humbertocosta.com.gigigoyoutubeplaylist.data.models.playlistitem.PlaylistItemListResponse;
import humbertocosta.com.gigigoyoutubeplaylist.utils.mvp.IBasePresenter;
import humbertocosta.com.gigigoyoutubeplaylist.utils.mvp.IBaseView;

/**
 * Created by humbertocosta on 28/01/18.
 */

interface PlaylistVideosContract {

    interface View extends IBaseView {

        void showVideos(PlaylistItemListResponse videos);
    }

    interface Presenter extends IBasePresenter<View> {

        void getVideos(Context context, String playlistId);
    }
}
