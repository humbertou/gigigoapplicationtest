package humbertocosta.com.gigigoyoutubeplaylist.ui.playlists;

import android.content.Context;

import java.util.List;

import humbertocosta.com.gigigoyoutubeplaylist.R;
import humbertocosta.com.gigigoyoutubeplaylist.data.models.playlist.PlaylistResponse;
import humbertocosta.com.gigigoyoutubeplaylist.data.services.DataRepository;
import humbertocosta.com.gigigoyoutubeplaylist.data.services.DataSource;
import humbertocosta.com.gigigoyoutubeplaylist.utils.mvp.BasePresenter;

/**
 * Created by humbertocosta on 28/01/18.
 */

public class PlaylistsPresenter extends BasePresenter<PlaylistsContract.View> implements
        PlaylistsContract.Presenter {

    private DataRepository dataRepository;

    public PlaylistsPresenter(PlaylistsContract.View view,
                              DataRepository dataRepository) {
        this.view = view;
        this.dataRepository = dataRepository;
    }

    @Override
    public void getPlaylists(Context context) {
        if (view == null) {
            return;
        }

        view.setProgressBar(true);

        dataRepository.getPlaylists(new DataSource.getPlaylistsCallback() {
            @Override
            public void onSuccess(PlaylistResponse playlistResponse) {
                if (view != null) {
                    view.showPlaylists(playlistResponse);
                    view.setProgressBar(false);
                }
            }

            @Override
            public void onFailure(Throwable throwable) {
                if (view != null) {
                    view.setProgressBar(false);
                    view.showToastMessage(context.getString(R.string.error_msg));
                }
            }

            @Override
            public void onNetworkFailure() {
                if (view != null) {
                    view.setProgressBar(false);
                    view.showToastMessage(context.getString(R.string.network_failure_msg));
                }
            }
        });
    }
}
