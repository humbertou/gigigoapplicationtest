package humbertocosta.com.gigigoyoutubeplaylist.ui.videos;

import android.content.Context;

import humbertocosta.com.gigigoyoutubeplaylist.R;
import humbertocosta.com.gigigoyoutubeplaylist.data.models.playlistitem.PlaylistItemListResponse;
import humbertocosta.com.gigigoyoutubeplaylist.data.services.DataRepository;
import humbertocosta.com.gigigoyoutubeplaylist.data.services.DataSource;
import humbertocosta.com.gigigoyoutubeplaylist.utils.mvp.BasePresenter;

/**
 * Created by humbertocosta on 28/01/18.
 */

public class PlaylistVideosPresenter extends BasePresenter<PlaylistVideosContract.View> implements
        PlaylistVideosContract.Presenter {

    private DataRepository dataRepository;

    public PlaylistVideosPresenter(PlaylistVideosContract.View view,
                                   DataRepository dataRepository) {
        this.view = view;
        this.dataRepository = dataRepository;
    }

    @Override
    public void getVideos(Context context, String playlistId) {
        if (view == null) {
            return;
        }

        view.setProgressBar(true);

        dataRepository.getPlaylistVideos(playlistId, new DataSource.getPlaylistVideosCallback() {
            @Override
            public void onSuccess(PlaylistItemListResponse response) {
                if (view != null) {
                    view.showVideos(response);
                    view.setProgressBar(false);
                }
            }

            @Override
            public void onFailure(Throwable throwable) {
                if (view != null) {
                    view.setProgressBar(false);
                    view.showToastMessage(context.getString(R.string.error_msg));
                }
            }

            @Override
            public void onNetworkFailure() {
                if (view != null) {
                    view.setProgressBar(false);
                    view.showToastMessage(context.getString(R.string.network_failure_msg));
                }
            }
        });
    }
}
