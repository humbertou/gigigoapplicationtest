package humbertocosta.com.gigigoyoutubeplaylist.ui.videos;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import humbertocosta.com.gigigoyoutubeplaylist.R;
import humbertocosta.com.gigigoyoutubeplaylist.data.models.playlist.Playlist;
import humbertocosta.com.gigigoyoutubeplaylist.data.models.playlist.PlaylistResponse;
import humbertocosta.com.gigigoyoutubeplaylist.data.models.playlistitem.PlaylistItem;
import humbertocosta.com.gigigoyoutubeplaylist.data.models.playlistitem.PlaylistItemListResponse;

/**
 * Created by humbertocosta on 26/01/18.
 */

public class PlaylistVideosRecyclerAdapter extends RecyclerView.Adapter<PlaylistVideosRecyclerAdapter.ViewHolder> {

    private ArrayList<PlaylistItem> videos;
    private Fragment fragment;

    public PlaylistVideosRecyclerAdapter(Fragment fragment, ArrayList<PlaylistItem> videos) {
        this.fragment = fragment;
        this.videos = videos;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTitle) TextView tvTitle;
        @BindView(R.id.tvDescription) TextView tvDescription;
        @BindView(R.id.ivThumbnail) ImageView ivThumbnail;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public PlaylistVideosRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.custom_row_video, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PlaylistVideosRecyclerAdapter.ViewHolder viewHolder, int position) {

        PlaylistItem video = videos.get(position);

        viewHolder.tvTitle.setText(video.getSnippet().getTitle());

        viewHolder.tvDescription.setText(video.getSnippet().getDescription());

        Picasso.with(fragment.getActivity()).
                load(video.getSnippet().getThumbnails().getMaxresThumbnail().getUrl()).
                into(viewHolder.ivThumbnail);
    }

    @Override
    public int getItemCount() {
        return videos.size();
    }

    public void clear() {
        int size = getItemCount();
        videos.clear();
        notifyItemRangeRemoved(0, size);
    }

    public void addAll(PlaylistItemListResponse videos) {
        int prevSize = getItemCount();
        this.videos.addAll(videos.getVideos());
        notifyItemRangeInserted(prevSize, videos.getVideos().size());
    }
}
