package humbertocosta.com.gigigoyoutubeplaylist.ui.videos;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.parceler.Parcels;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import humbertocosta.com.gigigoyoutubeplaylist.R;
import humbertocosta.com.gigigoyoutubeplaylist.data.models.playlist.Playlist;
import humbertocosta.com.gigigoyoutubeplaylist.data.models.playlistitem.PlaylistItem;
import humbertocosta.com.gigigoyoutubeplaylist.data.models.playlistitem.PlaylistItemListResponse;
import humbertocosta.com.gigigoyoutubeplaylist.data.services.DataRepository;
import humbertocosta.com.gigigoyoutubeplaylist.di.Injection;
import humbertocosta.com.gigigoyoutubeplaylist.utils.Config;
import humbertocosta.com.gigigoyoutubeplaylist.utils.mvp.BaseView;

/**
 * Created by humbertocosta on 28/01/18.
 */

public class PlaylistVideosFragment extends BaseView implements PlaylistVideosContract.View {

    @BindView(R.id.rvVideos) RecyclerView rvVideos;
    @BindView(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout;

    private PlaylistVideosContract.Presenter presenter;
    private PlaylistVideosRecyclerAdapter recyclerAdapter;

    private ArrayList<PlaylistItem> videos;
    private Playlist playlist;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            playlist = Parcels.unwrap(getArguments().getParcelable(Config.BUNDLE_PLAYLIST));
        }
        DataRepository dataRepository = Injection.provideDataRepository();
        presenter = new PlaylistVideosPresenter(this, dataRepository);
        videos = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_videos, container, false);
        ButterKnife.bind(this, view);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(playlist.getSnippet().getTitle());
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        recyclerAdapter = new PlaylistVideosRecyclerAdapter(this, videos);
        rvVideos.setAdapter(recyclerAdapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvVideos.setLayoutManager(linearLayoutManager);

        swipeRefreshLayout.setOnRefreshListener(() -> refreshVideos());

        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimaryDark, R.color.colorPrimary);

        getVideos();
    }

    private void getVideos() {
        presenter.getVideos(getContext().getApplicationContext(), playlist.getId());
    }

    private void refreshVideos() {
        getVideos();
    }

    @Override
    public void setProgressBar(boolean show) {
        swipeRefreshLayout.setRefreshing(show);
    }

    @Override
    public void showVideos(PlaylistItemListResponse videos) {
        recyclerAdapter.clear();
        this.videos = videos.getVideos();
        recyclerAdapter.addAll(videos);
    }
}
