package humbertocosta.com.gigigoyoutubeplaylist.ui.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import humbertocosta.com.gigigoyoutubeplaylist.R;
import humbertocosta.com.gigigoyoutubeplaylist.ui.playlists.PlaylistsFragment;
import humbertocosta.com.gigigoyoutubeplaylist.utils.BaseFragmentInteractionListener;
import humbertocosta.com.gigigoyoutubeplaylist.utils.FoaBaseActivity;

public class MainActivity extends FoaBaseActivity implements BaseFragmentInteractionListener {

    @BindView(R.id.toolbar) Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        showFragment(PlaylistsFragment.class);
    }
}
