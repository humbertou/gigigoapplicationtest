package humbertocosta.com.gigigoyoutubeplaylist.ui.playlists;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import humbertocosta.com.gigigoyoutubeplaylist.R;
import humbertocosta.com.gigigoyoutubeplaylist.data.models.playlist.Playlist;
import humbertocosta.com.gigigoyoutubeplaylist.data.models.playlist.PlaylistResponse;

/**
 * Created by humbertocosta on 26/01/18.
 */

public class PlaylistsRecyclerAdapter extends RecyclerView.Adapter<PlaylistsRecyclerAdapter.ViewHolder> {

    private ArrayList<Playlist> playlists;
    private Fragment fragment;

    public PlaylistsRecyclerAdapter(Fragment fragment, ArrayList<Playlist> playlists) {
        this.fragment = fragment;
        this.playlists = playlists;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTitle) TextView tvTitle;
        @BindView(R.id.ivThumbnail) ImageView ivThumbnail;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public PlaylistsRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.custom_row_playlist, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PlaylistsRecyclerAdapter.ViewHolder viewHolder, int position) {

        Playlist playlist = playlists.get(position);

        viewHolder.tvTitle.setText(playlist.getSnippet().getTitle());

        Picasso.with(fragment.getActivity()).
                load(playlist.getSnippet().getThumbnails().getMaxresThumbnail().getUrl()).
                into(viewHolder.ivThumbnail);
    }

    @Override
    public int getItemCount() {
        return playlists.size();
    }

    public void clear() {
        int size = getItemCount();
        playlists.clear();
        notifyItemRangeRemoved(0, size);
    }

    public void addAll(PlaylistResponse playlists) {
        int prevSize = getItemCount();
        this.playlists.addAll(playlists.getPlaylists());
        notifyItemRangeInserted(prevSize, playlists.getPlaylists().size());
    }
}
