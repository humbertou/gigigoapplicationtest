package humbertocosta.com.gigigoyoutubeplaylist.ui.playlists;

import android.content.Context;

import humbertocosta.com.gigigoyoutubeplaylist.data.models.playlist.PlaylistResponse;
import humbertocosta.com.gigigoyoutubeplaylist.utils.mvp.IBasePresenter;
import humbertocosta.com.gigigoyoutubeplaylist.utils.mvp.IBaseView;

/**
 * Created by humbertocosta on 28/01/18.
 */

interface PlaylistsContract {

    interface View extends IBaseView {

        void showPlaylists(PlaylistResponse playlists);
    }

    interface Presenter extends IBasePresenter<View> {

        void getPlaylists(Context context);
    }
}
