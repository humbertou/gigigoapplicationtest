package humbertocosta.com.gigigoyoutubeplaylist.ui.playlists;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.parceler.Parcels;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import humbertocosta.com.gigigoyoutubeplaylist.R;
import humbertocosta.com.gigigoyoutubeplaylist.data.models.playlist.Playlist;
import humbertocosta.com.gigigoyoutubeplaylist.data.models.playlist.PlaylistResponse;
import humbertocosta.com.gigigoyoutubeplaylist.data.services.DataRepository;
import humbertocosta.com.gigigoyoutubeplaylist.di.Injection;
import humbertocosta.com.gigigoyoutubeplaylist.ui.videos.PlaylistVideosFragment;
import humbertocosta.com.gigigoyoutubeplaylist.utils.BaseFragmentInteractionListener;
import humbertocosta.com.gigigoyoutubeplaylist.utils.Config;
import humbertocosta.com.gigigoyoutubeplaylist.utils.ItemClickSupport;
import humbertocosta.com.gigigoyoutubeplaylist.utils.mvp.BaseView;

/**
 * Created by humbertocosta on 28/01/18.
 */

public class PlaylistsFragment extends BaseView implements PlaylistsContract.View {

    @BindView(R.id.rvPlaylists) RecyclerView rvPlaylists;
    @BindView(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout;

    private PlaylistsContract.Presenter presenter;
    private PlaylistsRecyclerAdapter recyclerAdapter;
    private BaseFragmentInteractionListener fragmentInteractionListener;

    private ArrayList<Playlist> playlists;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DataRepository dataRepository = Injection.provideDataRepository();
        presenter = new PlaylistsPresenter(this, dataRepository);
        playlists = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_playlists, container, false);
        ButterKnife.bind(this, view);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Gigigo");
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        recyclerAdapter = new PlaylistsRecyclerAdapter(this, playlists);
        rvPlaylists.setAdapter(recyclerAdapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvPlaylists.setLayoutManager(linearLayoutManager);

        ItemClickSupport.addTo(rvPlaylists).setOnItemClickListener(
                (recyclerView, position, v) -> showDetailFragment(playlists.get(position)));

        swipeRefreshLayout.setOnRefreshListener(() -> refreshPlaylists());

        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimaryDark, R.color.colorPrimary);

        getPlaylists();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentInteractionListener = (BaseFragmentInteractionListener) getActivity();
    }

    private void getPlaylists() {
        presenter.getPlaylists(getContext().getApplicationContext());
    }

    @Override
    public void showPlaylists(PlaylistResponse playlist) {
        recyclerAdapter.clear();
        playlists = playlist.getPlaylists();
        recyclerAdapter.addAll(playlist);
    }

    private void refreshPlaylists() {
        getPlaylists();
    }

    @Override
    public void setProgressBar(boolean show) {
        swipeRefreshLayout.setRefreshing(show);
    }

    private void showDetailFragment(Playlist playlist) {
        Bundle bundle = new Bundle();
        Parcelable parcelable = Parcels.wrap(playlist);
        bundle.putParcelable(Config.BUNDLE_PLAYLIST, parcelable);
        fragmentInteractionListener.showFragment(PlaylistVideosFragment.class, bundle,
                true);
    }
}
