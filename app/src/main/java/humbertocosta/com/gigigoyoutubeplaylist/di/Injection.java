package humbertocosta.com.gigigoyoutubeplaylist.di;

import android.os.Build;

import java.util.concurrent.TimeUnit;

import humbertocosta.com.gigigoyoutubeplaylist.BuildConfig;
import humbertocosta.com.gigigoyoutubeplaylist.data.services.ApiService;
import humbertocosta.com.gigigoyoutubeplaylist.data.services.DataRepository;
import humbertocosta.com.gigigoyoutubeplaylist.data.services.RemoteDataSource;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by humbertocosta on 02/02/18.
 */

public class Injection {
    public static DataRepository provideDataRepository() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(chain -> {
            Request original = chain.request();

            Request request = original.newBuilder()
                    .header("Content-Type", "application/json")
                    .header("Accept", "application/json")
                    .method(original.method(), original.body())
                    .build();

            return chain.proceed(request);
        });

        httpClient.connectTimeout(100_000, TimeUnit.MILLISECONDS);
        httpClient.readTimeout(100_000, TimeUnit.MILLISECONDS);
        httpClient.writeTimeout(100_000, TimeUnit.MILLISECONDS);

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiService = retrofit.create(ApiService.class);

        return DataRepository.getInstance(
                RemoteDataSource.getInstance(apiService));
    }
}
