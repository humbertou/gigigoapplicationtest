package humbertocosta.com.gigigoyoutubeplaylist.utils.mvp;

/**
 * Created by humbertocosta on 28/01/18.
 */

public interface IBasePresenter<ViewT> {

    void onViewActive(ViewT view);

    void onViewInactive();
}
