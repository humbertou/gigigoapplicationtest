package humbertocosta.com.gigigoyoutubeplaylist.utils;


import android.os.Bundle;
import android.support.v4.app.Fragment;

public interface BaseFragmentInteractionListener {

    <T extends Fragment> void showFragment(Class<T> fragmentClass, Bundle bundle,
                                           boolean addToBackStack);
}
