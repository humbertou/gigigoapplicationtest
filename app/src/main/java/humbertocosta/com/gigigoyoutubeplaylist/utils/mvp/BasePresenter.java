package humbertocosta.com.gigigoyoutubeplaylist.utils.mvp;

/**
 * Created by humbertocosta on 28/01/18.
 */

public class BasePresenter<ViewT> implements IBasePresenter<ViewT> {

    protected ViewT view;

    @Override
    public void onViewActive(ViewT view) {
        this.view = view;
    }

    @Override
    public void onViewInactive() {
        view = null;
    }
}
