package humbertocosta.com.gigigoyoutubeplaylist.data.models.playlist;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import humbertocosta.com.gigigoyoutubeplaylist.data.models.base.BaseResponse;
import humbertocosta.com.gigigoyoutubeplaylist.data.models.base.PageInfo;

/**
 * Created by humbertocosta on 26/01/18.
 */

public class PlaylistResponse extends BaseResponse {

    @SerializedName("items")
    ArrayList<Playlist> playlists;

    public ArrayList<Playlist> getPlaylists() {
        return playlists;
    }

    public void setPlaylists(ArrayList<Playlist> playlists) {
        this.playlists = playlists;
    }
}
