package humbertocosta.com.gigigoyoutubeplaylist.data.models.base;

/**
 * Created by humbertocosta on 27/01/18.
 */

public class BaseItem {
    String id;
    String kind;
    String etag;
    Snippet snippet;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public Snippet getSnippet() {
        return snippet;
    }

    public void setSnippet(Snippet snippet) {
        this.snippet = snippet;
    }
}
