package humbertocosta.com.gigigoyoutubeplaylist.data.services;

import humbertocosta.com.gigigoyoutubeplaylist.data.models.playlist.PlaylistResponse;
import humbertocosta.com.gigigoyoutubeplaylist.data.models.playlistitem.PlaylistItemListResponse;

/**
 * Created by humbertocosta on 28/01/18.
 */

public abstract class DataSource {

    public interface getPlaylistsCallback {
        void onSuccess(PlaylistResponse playlistResponse);

        void onFailure(Throwable throwable);

        void onNetworkFailure();

    }

    public interface getPlaylistVideosCallback {

        void onSuccess(PlaylistItemListResponse playlistItemListResponse);

        void onFailure(Throwable throwable);

        void onNetworkFailure();

    }

    public abstract void getPlaylists(getPlaylistsCallback callback);

    public abstract void getPlaylistVideos(String playlistId, getPlaylistVideosCallback callback);
}
