package humbertocosta.com.gigigoyoutubeplaylist.data.services;


import humbertocosta.com.gigigoyoutubeplaylist.BuildConfig;
import humbertocosta.com.gigigoyoutubeplaylist.data.models.playlist.PlaylistResponse;
import humbertocosta.com.gigigoyoutubeplaylist.data.models.playlistitem.PlaylistItemListResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by humbertocosta on 02/02/18.
 */
public interface ApiService {
    String YOUTUBE = "youtube/";
    String API_VERSION = "v3/";

    String PLAYLISTS = YOUTUBE + API_VERSION + "playlists?" +
            "part=snippet" +
            "&channelId="+ BuildConfig.CHANNEL_ID +
            "&key="+ BuildConfig.API_KEY ;
    String PLAYLISTITEMS = YOUTUBE + API_VERSION + "playlistItems?" +
            "part=snippet" +
            "&key=" + BuildConfig.API_KEY +
            "&playlistId=%s";

    @GET(PLAYLISTS)
    Call<PlaylistResponse> getPlaylists();
    @GET
    Call<PlaylistItemListResponse> getVideos(@Url String url);
}
