package humbertocosta.com.gigigoyoutubeplaylist.data.services;

import android.content.Context;

import java.util.List;

import humbertocosta.com.gigigoyoutubeplaylist.data.models.playlist.PlaylistResponse;
import humbertocosta.com.gigigoyoutubeplaylist.data.models.playlistitem.PlaylistItem;
import humbertocosta.com.gigigoyoutubeplaylist.data.models.playlistitem.PlaylistItemListResponse;

/**
 * Created by humbertocosta on 28/01/18.
 */

public class DataRepository {

    private DataSource remoteDataSource;

    private static DataRepository dataRepository;

    public DataRepository(DataSource remoteDataSource) {
        this.remoteDataSource = remoteDataSource;
    }

    public static synchronized DataRepository getInstance(DataSource remoteDataSource) {
        if (dataRepository == null) {
            dataRepository = new DataRepository(remoteDataSource);
        }
        return dataRepository;
    }

    public void getPlaylists(final DataSource.getPlaylistsCallback callback) {
        remoteDataSource.getPlaylists(new DataSource.getPlaylistsCallback() {
            @Override
            public void onSuccess(PlaylistResponse playlistResponse) {
                callback.onSuccess(playlistResponse);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }

            @Override
            public void onNetworkFailure() {
                callback.onNetworkFailure();
            }
        });
    }

    public void getPlaylistVideos(String playlistId,
        final DataSource.getPlaylistVideosCallback callback) {
        remoteDataSource.getPlaylistVideos(playlistId, new DataSource.getPlaylistVideosCallback() {
            @Override
            public void onSuccess(PlaylistItemListResponse playlistItemListResponse) {
                callback.onSuccess(playlistItemListResponse);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }

            @Override
            public void onNetworkFailure() {
                callback.onNetworkFailure();
            }
        });
    }
}
