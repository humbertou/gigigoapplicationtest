package humbertocosta.com.gigigoyoutubeplaylist.data.models.playlistitem;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import humbertocosta.com.gigigoyoutubeplaylist.data.models.base.BaseResponse;

/**
 * Created by humbertocosta on 27/01/18.
 */

public class PlaylistItemListResponse extends BaseResponse {
    @SerializedName("items")
    ArrayList<PlaylistItem> videos;

    public ArrayList<PlaylistItem> getVideos() {
        return videos;
    }

    public void setVideos(ArrayList<PlaylistItem> videos) {
        this.videos = videos;
    }
}
