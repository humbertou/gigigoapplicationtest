package humbertocosta.com.gigigoyoutubeplaylist.data.models.base;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by humbertocosta on 26/01/18.
 */

public class Snippet {
    String publishedAt;
    String title;
    String description;
    Thumbnails thumbnails;

    public class Thumbnails {
        @SerializedName("default")
        Thumbnail defaultThumbnail;
        @SerializedName("high")
        Thumbnail highThumbnail;
        @SerializedName("maxres")
        Thumbnail maxresThumbnail;

        public Thumbnail getDefaultThumbnail() {
            return defaultThumbnail;
        }

        public Thumbnail getHighThumbnail() {
            return highThumbnail;
        }

        public Thumbnail getMaxresThumbnail() {
            return maxresThumbnail;
        }
    }

    public class Thumbnail {
        String url;
        int width;
        int height;

        public String getUrl() {
            return url;
        }

        public int getWidth() {
            return width;
        }

        public int getHeight() {
            return height;
        }
    }

    public String getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(String publishedAt) {
        this.publishedAt = publishedAt;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Thumbnails getThumbnails() {
        return thumbnails;
    }
}
