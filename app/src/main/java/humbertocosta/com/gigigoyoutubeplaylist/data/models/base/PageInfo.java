package humbertocosta.com.gigigoyoutubeplaylist.data.models.base;

/**
 * Created by humbertocosta on 27/01/18.
 */

public class PageInfo {
    int totalResults;
    int resultsPerPage;
}
