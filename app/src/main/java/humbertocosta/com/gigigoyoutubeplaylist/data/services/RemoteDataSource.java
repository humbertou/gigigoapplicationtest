package humbertocosta.com.gigigoyoutubeplaylist.data.services;

import java.util.HashMap;
import java.util.Map;

import humbertocosta.com.gigigoyoutubeplaylist.data.models.playlist.PlaylistResponse;
import humbertocosta.com.gigigoyoutubeplaylist.data.models.playlistitem.PlaylistItemListResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by humbertocosta on 02/02/18.
 */
public class RemoteDataSource extends DataSource {
    private static RemoteDataSource remoteDataSource;

    private ApiService apiService;

    private RemoteDataSource(ApiService apiService) {
        super();
        this.apiService = apiService;
    }

    public static synchronized RemoteDataSource getInstance(ApiService apiService) {
        if (remoteDataSource == null) {

            remoteDataSource = new RemoteDataSource(apiService);
        }
        return remoteDataSource;
    }

    @Override
    public void getPlaylists(final getPlaylistsCallback callback) {
        retrofit2.Call<PlaylistResponse> call =
                apiService.getPlaylists();

        call.enqueue(
                new Callback<PlaylistResponse>() {
                    @Override
                    public void onResponse(
                            Call<PlaylistResponse> call,
                            Response<PlaylistResponse> response) {
                        if (response.isSuccessful()) {
                            PlaylistResponse
                                    playlistResponse = response.body();
                            callback.onSuccess(playlistResponse);
                        } else {
                            callback.onFailure(new Throwable());
                        }
                    }

                    @Override
                    public void onFailure(
                            Call<PlaylistResponse> call,
                            Throwable t) {
                        callback.onFailure(t);
                    }
                });
    }

    @Override
    public void getPlaylistVideos(String playlistId, final getPlaylistVideosCallback callback) {
        Call<PlaylistItemListResponse> call =
                apiService.getVideos(String.format(ApiService.PLAYLISTITEMS, playlistId));

        call.enqueue(
                new Callback<PlaylistItemListResponse>() {
                    @Override
                    public void onResponse(
                            Call<PlaylistItemListResponse> call,
                            Response<PlaylistItemListResponse> response) {
                        if (response.isSuccessful()) {
                            PlaylistItemListResponse
                                    playlistItemListResponse = response.body();
                            callback.onSuccess(playlistItemListResponse);
                        } else {
                            callback.onFailure(new Throwable());
                        }
                    }

                    @Override
                    public void onFailure(
                            Call<PlaylistItemListResponse> call,
                            Throwable t) {
                        callback.onFailure(t);
                    }
                });
    }
}
