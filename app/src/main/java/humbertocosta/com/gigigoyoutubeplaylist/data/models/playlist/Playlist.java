package humbertocosta.com.gigigoyoutubeplaylist.data.models.playlist;

import org.parceler.Parcel;

import humbertocosta.com.gigigoyoutubeplaylist.data.models.base.BaseItem;

/**
 * Created by humbertocosta on 26/01/18.
 */

@Parcel(value = Parcel.Serialization.BEAN, analyze = {Playlist.class})
public class Playlist extends BaseItem {
}
